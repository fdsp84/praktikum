### Meeting Notes Zwischenstand 25.06.2020:

#### BLAST:
* positives entspricht in unserem Output der Coverage
* an Threshold E-value von 1e-50 rantasten und gucken, wie viel mehr Hits bei zB 1e-45 dazu kommen usw
* für jedes unserer 7 Proteine einzeln ausprobieren und Coverage - sowie Evalue - thresholds tabellarisch erfassen
* generell jeweils besten Hit je Spezies auswählen und auch für eine Isoform generell entscheiden (immer Isoform A bzw. 1 wäre angebracht)
* Skript schreiben zum schrittweisen Filtern der BLAST-Ergebnisse schreiben

#### Phylogenie:
* Splitstree Netzwerk, Examples anschauen
* bei Splitstree aus Alignment Baum machen und mit MEGA7 - Output vergleichen
* Fungi und Arabidopsis sind die Outgroup
* am besten Maximum Likelihood - Algorithmus für Konstruktion nehmen

### Recherche
* unsere Proteine hängen über NuRD zusammen, der deacetylierend agiert
* Domänen unserer Proteine abgleichen

### Drawio
* bedeutet Richtung der Pfeile etwas? Wenn nicht, bidirektionale Pfeile verwenden

### Nächste Schritte
##### Ouput von Blastall in tabellarischer Form
* Datenbank anlegen
* Tabelle 
    * Spalten:
`E-Value | Coverage | Identities | Gesamte Sequenz aus der Proteome (CLOB)`
* Beispielabfrage
    * `select sequence from results where e-value < e-50 and coverage > 60%;`

##### Multiple Alignments
* Pro Protein, beste Kombination von E-Value = e-50 und Coverage = 60%
    * Alignment per Hand durchführen
* Später Clustalo automatisch für E-Value und Coverage anstoßen

##### Philogenetischer Baum
