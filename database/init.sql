CREATE TABLE blast_results (
   query_id VARCHAR (200) NOT NULL,
   subject_id VARCHAR (200) NOT NULL,
   identity NUMERIC NOT NULL,
   alignment_length INTEGER NOT NULL,
   mismatches INTEGER NOT NULL,
   gap_openings INTEGER NOT NULL,
   query_start INTEGER NOT NULL,
   query_end INTEGER NOT NULL,
   subject_start INTEGER NOT NULL,
   subject_end INTEGER NOT NULL,
   expect_value NUMERIC NOT NULL,
   bit_score NUMERIC NOT NULL,
   coverage NUMERIC NOT NULL,
   abbreviation VARCHAR(5),
   subject_fasta_sequence TEXT
);

CREATE TABLE species (
   acronym VARCHAR (200) PRIMARY KEY,
   name VARCHAR (200) NOT NULL
);