| Protein      | Domain IDs |  PFAM Acc. Nr | 
| ----------- | ----------- | ------------ |
| MBD2        | MBD_C        |PF14048.6  |
|             | MBDa          |PF16564.5 |  
|            |  MBD         | PF01429.19 |   
 |MBD3       | MBD_C       |PF14048.6  |
|            |     MBDa   |PF16564.5  | 
|MTA1       |MTA_R1	|PF17226.2|
|           |BAH	|PF01426.18|
|            |ELM2	|PF01448.24|
|            |GATA	|PF00320.27|
|            |Myb_DNA-binding|	PF00249.31|
|MTA2   	|MTA_R1	|PF17226.2|
|	        |BAH	|PF01426.18|
|	        |ELM2	|PF01448.24|
|	        |GATA	|PF00320.27|
|MTA3	    |BAH	|PF01426.18|
|       	|MTA_R1	|PF17226.2|
|	        |ELM2	|PF01448.24|
|	        |GATA	|PF00320.27|
|          |Myb_DNA-binding	|PF00249.31|
|RBBP4	    |CAF1C_H4-bd     |  PF12265.8
|         	|WD40	        | PF00400.32 |
|RBBP7	    |CAF1C_H4-bd	|PF12265.8 |
|        	|WD40         	|PF00400.32 |




