hmmscan results for job 5D9C509E-BB83-11EA-89C8-0C7A53F04F9B.7

Family-Id Family-Accession Clan Start End Ali-Start Ali-End Model-Start Model-End Bit-Score Ind.-E-value Cond.-E-value Description
==================================================================================================================================

CAF1C_H4-bd PF12265.8 - 62 131 62 131 1 67 89.16 1.6e-25 2.6e-29 Histone-binding protein RBBP4 or subunit C of CAF1 complex

---------------------------------------------------------------------------------------------------------------------------------

MODEL  eeylewkknapfLYdmlhshslewPsLsfdwlPdtvs...s.lskqrllvGtqtsgkeqnylyvakvslpk
MATCH  eey++wkkn+pfLYd++++h+l+wPsL+++wlP+ ++   + +  + l++Gt+ts +eqn+l+va+v++p+
PPL    7********************************95446888899999999****9.9***********996
SEQ    EEYKIWKKNTPFLYDLVMTHALQWPSLTVQWLPEVTKpegKdYALHWLVLGTHTS-DEQNHLVVARVHIPN

==================================================================================================================================

WD40 PF00400.32 CL0186 210 249 225 249 15 38 13.53 0.1 1.7e-05 WD domain, G-beta repeat

---------------------------------------------------------------------------------------------------------------------------------

MODEL  slafspdgaw...lasGsdDgtvriWd
MATCH  +l+++++  +   l+s+sdD+tv +Wd
PPL    4432222..1334568999*******9
SEQ    GLSWNSN--LsghLLSASDDHTVCLWD

==================================================================================================================================

WD40 PF00400.32 CL0186 261 299 265 299 6 38 14.30 0.058 9.6e-06 WD domain, G-beta repeat

---------------------------------------------------------------------------------------------------------------------------------

MODEL  tltGHss.vtslafspdgawlasGs..dDgtvriWd
MATCH  ++tGHs  v ++a++  +  l+ Gs  dD++++iWd
PPL    79***7767***5544444488.76458*******9
SEQ    IFTGHSAvVEDVAWHLLHESLF-GSvaDDQKLMIWD

==================================================================================================================================

WD40 PF00400.32 CL0186 306 345 308 345 3 38 28.41 2.0e-06 3.4e-10 WD domain, G-beta repeat

---------------------------------------------------------------------------------------------------------------------------------

MODEL  clrtltGH.ssvtslafspdgawl.asGsdDgtvriWd
MATCH  + +  ++H   v++l+f+p++ ++ a+Gs D+tv +Wd
PPL    5677789*666************6379**********9
SEQ    PSHLVDAHtAEVNCLSFNPYSEFIlATGSADKTVALWD

==================================================================================================================================

WD40 PF00400.32 CL0186 351 389 353 389 4 38 21.20 0.00038 6.4e-08 WD domain, G-beta repeat

---------------------------------------------------------------------------------------------------------------------------------

MODEL  lrtltGHss.vtslafspdgawla.sGsdDgtvriWd
MATCH  l+t++ H++ +  + +sp++  ++ s++ D+ + +Wd
PPL    8******556************7525567*******9
SEQ    LHTFESHKDeIFQVHWSPHNETILaSSGTDRRLNVWD

==================================================================================================================================

WD40 PF00400.32 CL0186 407 446 410 445 4 37 19.68 0.0012 1.9e-07 WD domain, G-beta repeat

---------------------------------------------------------------------------------------------------------------------------------

MODEL  lrtltGH.ssvtslafspdgawlasG.sdDgtvriW
MATCH  l +  GH   +++++++p+ +w++++ s+D+   iW
PPL    55667**666************9977799*******
SEQ    LFIHGGHtAKISDFSWNPNEPWVICSvSEDNIMQIW

==================================================================================================================================


Search Details
==============
Date Started: 2020-07-01 11:12:27
Cmd: hmmscan --cut_ga  --hmmdb pfam 
Database: pfam,  version 32.0,  downloaded on 2019-10-01
Query:  >NP_001185648.1 histone-binding protein RBBP7
 isoform 1 [Homo sapiens]
 MAAEAGVVGAGASPDGDWRDQACGLLLHVHLSSRLGRAAPVRTGRHLR
 TVFEDTVEERVINEEYKIWKKNTPFLYDLVMTHALQWPSLTVQWLPEV
 TKPEGKDYALHWLVLGTHTSDEQNHLVVARVHIPNDDAQFDASHCDSD
 KGEFGGFGSVTGKIECEIKINHEGEVNRARYMPQNPHIIATKTPSSDV
 LVFDYTKHPAKPDPSGECNPDLRLRGHQKEGYGLSWNSNLSGHLLSAS
 DDHTVCLWDINAGPKEGKIVDAKAIFTGHSAVVEDVAWHLLHESLFGS
 VADDQKLMIWDTRSNTTSKPSHLVDAHTAEVNCLSFNPYSEFILATGS
 ADKTVALWDLRNLKLKLHTFESHKDEIFQVHWSPHNETILASSGTDRR
 LNVWDLSKIGEEQSAEDAEDGPPELLFIHGGHTAKISDFSWNPNEPWV
 ICSVSEDNIMQIWQMAENIYNDEESDVTTSELEGQGS

Stats
=====
nhits:3
elapsed:0.04
Z:17929
Z_setby:0
n_past_msv:369
unpacked:3
user:0
domZ_setby:0
nseqs:1
n_past_bias:330
sys:0
n_past_fwd:4
nmodels:17929
nincluded:3
n_past_vit:27
nreported:3
domZ:3

