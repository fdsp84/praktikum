# hmmsearch :: search profile(s) against a sequence database
# HMMER 3.3 (Nov 2019); http://hmmer.org/
# Copyright (C) 2019 Howard Hughes Medical Institute.
# Freely distributed under the BSD open source license.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# query HMM file:                  ../Results/HMM/Profiles/GATA.hmm
# target sequence database:        ../Results/Clustalw_Inputs/sp_Q9BTC8_2_MTA3_HUMAN.out
# output directed to file:         ../Results/HMM/Outputs/GATA-MTA3.hmmout
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Query:       GATA  [M=36]
Accession:   PF00320.28
Description: GATA zinc finger
Scores for complete sequences (score includes all domains):
   --- full sequence ---   --- best 1 domain ---    -#dom-
    E-value  score  bias    E-value  score  bias    exp  N  Sequence                 Description
    ------- ------ -----    ------- ------ -----   ---- --  --------                 -----------
    1.8e-12   36.9   7.8    3.7e-12   35.9   7.8    1.6  1  sp|Q9BTC8.2|MTA3_HUMAN    RecName: Full=Metastasis-associated
      1e-11   34.5   6.5    2.6e-11   33.2   6.5    1.7  1  Drosophila_melanogaster   NP_730976.1  metastasis associated 
    2.4e-08   23.7   5.4    9.8e-08   21.8   5.4    2.1  1  Orbicella_faveolata       XP_020603577.1  metastasis-associat
      3e-08   23.4   5.4    9.6e-08   21.8   5.4    1.9  1  Pocillopora_damicornis    XP_027051311.1  metastasis-associat
    3.1e-08   23.4   5.4    9.6e-08   21.8   5.4    1.9  1  Stylophora_pistillata     XP_022784370.1  metastasis-associat
    9.5e-08   21.8   5.4    9.5e-08   21.8   5.4    2.4  2  Acropora_millepora        XP_029201970.1  metastasis-associat
    9.7e-08   21.8   9.2    2.9e-07   20.3   9.2    1.9  1  Hofstenia_miamia          HMIM000285-PA  pep scaffold:HmiaM1:
    1.1e-07   21.7   7.0    3.8e-07   19.9   7.0    2.0  1  Exaiptasia_pallida        XP_020909215.1  metastasis-associat
    2.5e-07   20.5   6.7    8.6e-07   18.8   6.7    2.0  1  Actinia_tenebrosa         XP_031554089.1  metastasis-associat
    9.8e-07   18.6   8.6      4e-06   16.6   8.6    2.2  1  Hydra_vulgaris            XP_002167188.2  PREDICTED: metastas
    1.2e-05   15.2   5.8    1.2e-05   15.2   5.8    1.8  1  Amphimedon_queenslandica  XP_003383835.3  PREDICTED: metastas
    3.8e-05   13.5   9.0    0.00015   11.6   9.0    2.1  1  Nematostella_vectensis    XP_032224579.1  metastasis-associat
    7.7e-05   12.5  16.5    1.9e-05   14.5  12.7    2.0  2  Trichoplax_adherens       XP_002118020.1  hypothetical protei
    0.00012   11.9  10.9    3.8e-05   13.5   5.7    2.7  2  Sycon_ciliatum            scpid34929|  scgid14320| Metastasis
    0.00027   10.8  17.5    3.1e-05   13.8  12.7    1.9  2  Trichoplax_sp._H2         RDD37744.1  Metastasis-associated p
     0.0023    7.8  14.2    1.4e-05   14.9   5.0    2.2  1  Dendronephthya_gigantea   XP_028398745.1  metastasis-associat


Domain annotation for each sequence (and alignments):
>> sp|Q9BTC8.2|MTA3_HUMAN  RecName: Full=Metastasis-associated protein MTA3
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   35.9   7.8   3.5e-12   3.7e-12       1      35 [.     379     415 ..     379     416 .. 0.97

  Alignments for each domain:
  == domain 1  score: 35.9 bits;  conditional E-value: 3.5e-12
                    GATA   1 CsnCgttkTplWRrg..pngnksLCnaCGlyykkkgl 35 
                             C++C +t++ +W+++  pn++++LC  C+ly+kk+g 
  sp|Q9BTC8.2|MTA3_HUMAN 379 CESCYATQSHQWYSWgpPNMQCRLCAICWLYWKKYGG 415
                             ***********************************96 PP

>> Drosophila_melanogaster  NP_730976.1  metastasis associated 1-like, isoform A [Drosophila melanogaster]
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   33.2   6.5   2.5e-11   2.6e-11       1      36 []     459     496 ..     459     496 .. 0.94

  Alignments for each domain:
  == domain 1  score: 33.2 bits;  conditional E-value: 2.5e-11
                     GATA   1 CsnCgttkTplWRrg..pngnksLCnaCGlyykkkglk 36 
                              C++Cgttk+++W++   ++ + +LC +C+ y++ +g++
  Drosophila_melanogaster 459 CESCGTTKSSQWNSVssGHSTSRLCLSCWEYWRRYGSM 496
                              **************988888899************986 PP

>> Orbicella_faveolata  XP_020603577.1  metastasis-associated protein MTA3-like [Orbicella faveolata]
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   21.8   5.4   9.2e-08   9.8e-08       1      35 [.     377     415 ..     377     416 .. 0.88

  Alignments for each domain:
  == domain 1  score: 21.8 bits;  conditional E-value: 9.2e-08
                 GATA   1 CsnCgttkTplWRrg..pngnks..LCnaCGlyykkkgl 35 
                          C++C  t++ +W+ +  pn +    LC+ C++y+kk+g 
  Orbicella_faveolata 377 CESCYGTSSQQWYPWgvPNSQHKfwLCTLCWIYWKKYGG 415
                          ***************9966665455************96 PP

>> Pocillopora_damicornis  XP_027051311.1  metastasis-associated protein MTA1-like [Pocillopora damicornis]
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   21.8   5.4     9e-08   9.6e-08       1      35 [.     377     415 ..     377     416 .. 0.88

  Alignments for each domain:
  == domain 1  score: 21.8 bits;  conditional E-value: 9e-08
                    GATA   1 CsnCgttkTplWRrg..pngnks..LCnaCGlyykkkgl 35 
                             C++C  t++ +W+ +  pn +    LC+ C++y+kk+g 
  Pocillopora_damicornis 377 CESCYGTSSQQWYPWgvPNSQHKfwLCTLCWIYWKKYGG 415
                             ***************9966665455************96 PP

>> Stylophora_pistillata  XP_022784370.1  metastasis-associated protein MTA3-like [Stylophora pistillata]
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   21.8   5.4     9e-08   9.6e-08       1      35 [.     377     415 ..     377     416 .. 0.88

  Alignments for each domain:
  == domain 1  score: 21.8 bits;  conditional E-value: 9e-08
                   GATA   1 CsnCgttkTplWRrg..pngnks..LCnaCGlyykkkgl 35 
                            C++C  t++ +W+ +  pn +    LC+ C++y+kk+g 
  Stylophora_pistillata 377 CESCYGTSSQQWYPWgvPNSQHKfwLCTLCWIYWKKYGG 415
                            ***************9966665455************96 PP

>> Acropora_millepora  XP_029201970.1  metastasis-associated protein MTA3-like [Acropora millepora]
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   21.8   5.4     9e-08   9.5e-08       1      35 [.     377     415 ..     377     416 .. 0.88
   2 ?   -2.0   0.1       2.5       2.7      22      27 ..     700     705 ..     696     706 .. 0.84

  Alignments for each domain:
  == domain 1  score: 21.8 bits;  conditional E-value: 9e-08
                GATA   1 CsnCgttkTplWRrg..pngnks..LCnaCGlyykkkgl 35 
                         C++C  t++ +W+ +  pn +    LC+ C++y+kk+g 
  Acropora_millepora 377 CESCYGTSSQQWYPWgvPNSQHKfwLCTLCWIYWKKYGG 415
                         ***************9966665455************96 PP

  == domain 2  score: -2.0 bits;  conditional E-value: 2.5
                GATA  22 LCnaCG 27 
                          Cn CG
  Acropora_millepora 700 ECNYCG 705
                         6****9 PP

>> Hofstenia_miamia  HMIM000285-PA  pep scaffold:HmiaM1:SCFE01000010.1:2827034:2848956:-1 gene:HMIM000285 transcript:HMI
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   20.3   9.2   2.8e-07   2.9e-07       1      34 [.     430     465 ..     430     467 .. 0.92

  Alignments for each domain:
  == domain 1  score: 20.3 bits;  conditional E-value: 2.8e-07
              GATA   1 CsnCgttkTp.lWRrg.pngnksLCnaCGlyykkkg 34 
                       C+ C tt++p  W+ + pn+ ++LC +C+ly+kk g
  Hofstenia_miamia 430 CEGCSTTQSPgGWYMWgPNNLCRLCDPCWLYWKKLG 465
                       9********737***99*****************87 PP

>> Exaiptasia_pallida  XP_020909215.1  metastasis-associated protein MTA3 [Exaiptasia pallida]
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   19.9   7.0   3.5e-07   3.8e-07       1      35 [.     378     416 ..     378     417 .. 0.86

  Alignments for each domain:
  == domain 1  score: 19.9 bits;  conditional E-value: 3.5e-07
                GATA   1 CsnCgttkTplWRrg.pngnks...LCnaCGlyykkkgl 35 
                         C+ C  t++ +W+ + pn++++   LC+ C++y+kk+g 
  Exaiptasia_pallida 378 CEGCFGTTSQQWYPWsPNNSPHklwLCTLCWIYWKKYGG 416
                         9***************666555555************96 PP

>> Actinia_tenebrosa  XP_031554089.1  metastasis-associated protein MTA3-like isoform X1 [Actinia tenebrosa]
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   18.8   6.7   8.1e-07   8.6e-07       1      35 [.     378     416 ..     378     417 .. 0.84

  Alignments for each domain:
  == domain 1  score: 18.8 bits;  conditional E-value: 8.1e-07
               GATA   1 CsnCgttkTplWRrg.pngnks...LCnaCGlyykkkgl 35 
                        C+ C  t++ +W+ + pn++ +   LC+ C++y+kk+g 
  Actinia_tenebrosa 378 CEGCFGTSSQQWYPWsPNNSQHklwLCTLCWIYWKKYGG 416
                        99*************9555443444************96 PP

>> Hydra_vulgaris  XP_002167188.2  PREDICTED: metastasis-associated protein MTA3-like [Hydra vulgaris]
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   16.6   8.6   3.8e-06     4e-06       1      34 [.     402     439 ..     402     441 .. 0.83

  Alignments for each domain:
  == domain 1  score: 16.6 bits;  conditional E-value: 3.8e-06
            GATA   1 CsnCgttkTplWRrg....pngnksLCnaCGlyykkkg 34 
                     C++C t+++ lW+ +     + +  LC+ C++y+kk g
  Hydra_vulgaris 402 CESCFTKESVLWYPWgqssSQCKLYLCKECWVYWKKCG 439
                     ***************6654444556**********966 PP

>> Amphimedon_queenslandica  XP_003383835.3  PREDICTED: metastasis-associated protein MTA3-like isoform X1 [Amphimedon q
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   15.2   5.8   1.1e-05   1.2e-05       1      35 [.     391     427 ..     391     428 .. 0.96

  Alignments for each domain:
  == domain 1  score: 15.2 bits;  conditional E-value: 1.1e-05
                      GATA   1 CsnCgttkTplWRrg..pngnksLCnaCGlyykkkgl 35 
                               C++C+ t ++ W ++   + +++LC  C +y++k+g 
  Amphimedon_queenslandica 391 CESCRITVSTRWVSWgpAHDHCRLCGHCYVYWRKYGG 427
                               ***************99999***************96 PP

>> Nematostella_vectensis  XP_032224579.1  metastasis-associated protein MTA3 [Nematostella vectensis]
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   11.6   9.0   0.00014   0.00015       1      35 [.     324     362 ..     324     363 .. 0.84

  Alignments for each domain:
  == domain 1  score: 11.6 bits;  conditional E-value: 0.00014
                    GATA   1 CsnCgttkTplWRrg.pngnks...LCnaCGlyykkkgl 35 
                             C+ C   ++ +W+ + p+++ +   LC+ C++y+kk+g 
  Nematostella_vectensis 324 CEGCYGCTSQQWYPWsPSNSQHkfwLCTLCWIYWKKYGG 362
                             999************8554444556************96 PP

>> Trichoplax_adherens  XP_002118020.1  hypothetical protein TRIADDRAFT_33395, partial [Trichoplax adhaerens]
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 ?   -2.5   0.1       3.7       3.9       4      14 ..     163     173 ..     162     175 .. 0.82
   2 !   14.5  12.7   1.8e-05   1.9e-05       1      32 [.     367     400 ..     367     402 .. 0.97

  Alignments for each domain:
  == domain 1  score: -2.5 bits;  conditional E-value: 3.7
                 GATA   4 CgttkTplWRr 14 
                          C   +T +W +
  Trichoplax_adherens 163 CEDVETLQWTS 173
                          88899****66 PP

  == domain 2  score: 14.5 bits;  conditional E-value: 1.8e-05
                 GATA   1 CsnCgttkTplWRrg..pngnksLCnaCGlyykk 32 
                          C nC  t+++ W+ +   n n+ LC +C+ y+kk
  Trichoplax_adherens 367 CINCSITSSTSWHTWnvANSNCKLCSNCWWYWKK 400
                          99*******************************9 PP

>> Sycon_ciliatum  scpid34929|  scgid14320| Metastasis-associated protein MTA3
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   13.5   5.7   3.6e-05   3.8e-05       1      34 [.     390     427 ..     390     429 .. 0.84
   2 ?   -0.3   0.2      0.73      0.78      23      33 ..     448     458 ..     441     461 .. 0.71

  Alignments for each domain:
  == domain 1  score: 13.5 bits;  conditional E-value: 3.6e-05
            GATA   1 CsnCgttkTplWRrg.....pngnksLCnaCGlyykkkg 34 
                     C+ C +t++  W+++      n ++ LC +C+ y+  +g
  Sycon_ciliatum 390 CEGCPATSSVRWHKWnhggdTNAQP-LCSPCWEYWMRYG 427
                     999***********96666445555.*********9887 PP

  == domain 2  score: -0.3 bits;  conditional E-value: 0.73
            GATA  23 CnaCGlyykkk 33 
                     Cn CG  +  k
  Sycon_ciliatum 448 CNVCGKTFSRK 458
                     99999766554 PP

>> Trichoplax_sp._H2  RDD37744.1  Metastasis-associated protein MTA1 [Trichoplax sp. H2]
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 ?   -3.4   0.1       6.8       7.2       4      14 ..     232     242 ..     232     244 .. 0.81
   2 !   13.8  12.7   2.9e-05   3.1e-05       1      32 [.     436     469 ..     436     471 .. 0.97

  Alignments for each domain:
  == domain 1  score: -3.4 bits;  conditional E-value: 6.8
               GATA   4 CgttkTplWRr 14 
                        C   +T +W +
  Trichoplax_sp._H2 232 CEDVETLQWTS 242
                        88889****66 PP

  == domain 2  score: 13.8 bits;  conditional E-value: 2.9e-05
               GATA   1 CsnCgttkTplWRrg..pngnksLCnaCGlyykk 32 
                        C nC  t+++ W+ +   n n+ LC +C+ y+kk
  Trichoplax_sp._H2 436 CINCSITSSTSWHTWnvANSNCKLCSNCWWYWKK 469
                        99*******************************9 PP

>> Dendronephthya_gigantea  XP_028398745.1  metastasis-associated protein MTA3-like [Dendronephthya gigantea]
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !   14.9   5.0   1.3e-05   1.4e-05       1      35 [.     383     421 ..     383     422 .. 0.84

  Alignments for each domain:
  == domain 1  score: 14.9 bits;  conditional E-value: 1.3e-05
                     GATA   1 CsnCgttkTplWRrg..pngnks..LCnaCGlyykkkgl 35 
                              C++C + ++++W  +  pn ++   +C  C++y+kk+g 
  Dendronephthya_gigantea 383 CESCYANTSTHWFPWstPNTQYKhyVCSVCWVYWKKYGG 421
                              ***************9955543233************96 PP



Internal pipeline statistics summary:
-------------------------------------
Query model(s):                            1  (36 nodes)
Target sequences:                         17  (13445 residues searched)
Passed MSV filter:                        16  (0.941176); expected 0.3 (0.02)
Passed bias filter:                       16  (0.941176); expected 0.3 (0.02)
Passed Vit filter:                        16  (0.941176); expected 0.0 (0.001)
Passed Fwd filter:                        16  (0.941176); expected 0.0 (1e-05)
Initial search space (Z):                 17  [actual number of targets]
Domain search space  (domZ):              16  [number of targets reported over threshold]
# CPU time: 0.00u 0.00s 00:00:00.00 Elapsed: 00:00:00.00
# Mc/sec: 94.78
//
[ok]
