## RBBP4 and RBBP76 

#### Both proteins belongs to Core PRC2 Complex (see Schuettengruber 2017)
* PcG Complex Components in Mammals
    * **RBBP4/7**
* PcG Complex Components in Flies
    * **Nurf55/Caf1**
* Characteristic Domain
    * **WD40**
* (Epigenetic) Function
    * **H3K36me3 binding**
        * H3K36me3 is an epigenetic modification to the DNA packaging protein **Histone H3**. 
        It is a mark that indicates the **tri-methylation** at the **36th lysine residue of 
        the histone H3** protein and often associated with gene bodies. 
        There are diverse modifications at H3K36 and have many important 
        biological processes.
    


#### RBBP4
##### Summary
RBBP4 (RB Binding Protein 4, Chromatin Remodeling Factor) is a Protein Coding gene.
    Diseases associated with RBBP4 include Retinoblastoma and Hemophagocytic
    Lymphohistiocytosis, Familial, 1. Among its related pathways are Activated PKN1
    stimulates transcription of AR (androgen receptor) regulated genes KLK2 and KLK3
    and Hedgehog signaling events mediated by Gli proteins.

##### Isoform
We identified **three** isoforms. We will use [RBBP4-Isoform-A](RBBP4-isoform-A.fasta) since 
it contains the most number of aminoacids (**425**).

#### RBBP7
##### Summary
RBBP7 (RB Binding Protein 7, Chromatin Remodeling Factor) is a Protein Coding gene.
    Diseases associated with RBBP7 include Retinoblastoma and Transitional Papilloma.
    Among its related pathways are Chromatin organization and Activated PKN1 stimulates
    transcription of AR (androgen receptor) regulated genes KLK2 and KLK3.

##### Isoform
We identified **two** isoforms. We will use [RBBP7-Isoform-1](RBBP7-isoform-1.fasta) since 
it contains the most number of aminoacids (**469**).
