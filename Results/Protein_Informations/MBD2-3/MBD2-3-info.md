#### Methyl-CpG-binding domain proteins MBD2 and MBD3

- paralog genes
- nuclear proteins
- bind CpG Islands
- may function as mediators of the biological consequences of the methylation signal
- recruit the NuRD complex (MBD2/NuRD and MBD3/NuRD)
- linked to additional domains associated with chromatin, such as the bromodomain, the AT hook motif,the SET domain, or the PHD finger (MBD domain)
- MBD-containing proteins appear to act as structural proteins, which recruit a variety of histone deacetylase (HDAC) complexes and chromatin remodeling factors, leading to chromatin compaction and, consequently, to transcriptional repression

- MBD2
    - capable of binding specifically to methylated DNA
    - can also repress transcription from methylated gene promoters
    - Gene Ontology anntoations: chromatin binding, protein domain specific binding
    - gene silencing

- MBD3
    - mediates the association of metastasis-associated protein 2 (MTA2) with the core histone deacetylase complex
    - does not bind methylated DNA
    - Gene Ontology anntoations: RNA polymerase II proximal promoter sequence-specific DNA binding, chromatin binding
