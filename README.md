### Find our report here: [Final_Report_Group7_Bioinformatics2020.pdf](Final_Report_Group7_Bioinformatics2020.pdf)

## Technical Objectives
**From** [Query Proteins](Results/Proteine_fuer_die_Gruppen.readme) and [Proteomes](Proteome) 
**To** [Alignments](Results/Alignments/Images), [HMMER Ouputs](Results/HMM/Outputs), 
[Splitstree Trees](Results/Splitstree_Trees/) and [Heat Maps of HMMER Results](Results/Heat_Maps_Domains)

## How to run the scripts

#### Download proteome and create blast databases
* **Input**: [proteomTable.txt](Proteome/proteomTable.txt)
* Go to [proteome_database_creation.md](proteome_database_creation.md)
* **Run**:
    * cd `Proteome`
    * [run_blast_commands.sh](run_blast_commands.sh)
* **Output**: [Blast Databases in this Folder](Proteome) 

#### Create SQL database tables and filling them up
* **Input**: [Blast Databases in this Folder](Proteome) 
* Go to [filling_up_database_table.md](filling_up_database_table.md)
* **Output**: 
    * Table **Blast_Results** with additional columns `coverage` and `fasta sequence` in each entry
    * Table **Species** with abbreviation (e.g. NVEC) of species and its full scientific name (e.g. Nematostella vectensis)


#### Query Database and Run ClustalW Pipeline
* **Input**:
    * Table **Blast_Results** with additional columns `coverage` and `fasta sequence` in each entry
    * Table **Species** with abbreviation (e.g. NVEC) of species and its full scientific name (e.g. Nematostella vectensis)
    * [Thresholds](thresholds_by_protein.md) for each protein 
* **Run**: [run_pipeline_for_proteins.sh](src/run_pipeline_for_proteins.sh)
    * We run clustalw both without and with the flag ` -output=NEXUS`, 
    since Splitstree cannot open some `.aln` files, see [Daily Report](daily_report.md)
    
* **Output**: 
    * [Candidate Proteins .out Files](Results/Candidate_Proteins):
        * These are the hit sequences for each query protein as **clustalw** inputs. In one or two cases, there were 
    duplicated proteins and **clustalw** had problems with that. So, we removed these duplicated proteins.
        * We replaced the query protein ids with their **species scientific name**
    * [.aln Files](Results/Alignments)
    * [.nxs Files](Results/Nexus)
    * [Alignment Images](Results/Alignments/Images): Created manually

#### Splitstree
 * **Input**:
    * [.aln Files](Results/Alignments)
    * [.nxs Files](Results/Nexus)
* **Run**:
    * Manually with many bugs, see [Daily Report](daily_report.md)
* **Output**: [Splitstree Trees](Results/Splitstree_Trees/)

#### Execute Hmmer with Downloaded Profiles
* **Input**:
    * [Candidate Proteins .out Files](Results/Candidate_Proteins): These are the hit sequences for each query protein as **clustalw** inputs (multi fasta)
    from last step
    * [HMMER Profiles for Domains](Results/HMM/Profiles) from [PFAM](http://pfam.xfam.org/)
* **Run**:
    * [run_hmmsearch_for_proteins.sh](src/run_hmmsearch_for_proteins.sh)
        * **hmmsearch** for each domain against its protein
* **Output**: [Hmmer Output als Tables](Results/HMM/Outputs)
    * That means, we run **hmmer** against the subject proteins for each query protein that are used to build our 
    Splitstree tree 


#### Create Heat Maps from Hmmer Outputs
* **Input**:
    * [Hmmer Output als Tables](Results/HMM/Outputs) 
* **Run**:
    * [heat_map_creation.py](src/heat_map_creation.py)
    * [heat_map_all_protein_same_figure_creation.py](src/heat_map_all_protein_same_figure_creation.py)
* **Output**:
    * [Heat Maps for Each Protein](Results/Heat_Maps_Domains) with species ordering from [Species_List_for_Heatmap.txt](Results/Heat_Maps_Domains/Species_List_for_Heatmap.txt)
    * [One Heat Map with All Proteins](Results/Heat_Maps_Domains/All_Proteins_Normalized.png) Normalized Values