## TODOs

#### Pipeline fertig machen
* Coverage ins Hauptskript einbinden **DONE**
* ClustalW in die Pipeline einbinden **DONE**

#### Für jedes Protein die Pipeline anwenden (Splitstree)
* Verschiedene Coverage und E-Value Thresholds ansetzen **DONE**
* Beste Parameter ermitteln **DONE**
* Isoforms der Zielsequenzen rausnehmen **DONE**
* Output mit besten Parametern ins Splitstree einspeisen **DONE**
* Remove duplicated isoforms automatically `later`
* Rename labels automatically **DONE**
    * Get species name from proteome database **DONE**
* Generate `.aln` and new trees **DONE**
* Analyse sequence alignments (**ClustalX**) `.aln` files -> Splitstree 
    * ggb. cut off parts that were not matched (optional)
* Vergleicht unbedingt auch die mit Splitstree erstellten Baeume fuer alle Proteine und bringt diese in Zusammenhang
* Es ist ausgesprochen wichtig, sich bewusst zu machen, wie (a) ein normaler 
phylogenetischer Baum (wie der vorgegebene) und (b) ein mit Splitstree erstellter 
Baum zu lesen ist. Macht Euch noch einmal klar, dass durch die Inklusion einer
zusaetzlichen Dimension mehr Informationen in einem mit Splitstree enthaltenen Baum
konserviert ist **Done?**

#### Hidden Markov Models (HMMer)
* PFAM -> Download Domain Profile für every protein **DONE**
* execute HMER with downloaded profiles **DONE**
* **Heatmap** analogous to Schuettengruber **DONE**
    * HMER Ergebnisse in Zusammenhang bringen **DONE**
        * Heatmaps **DONE**
* Anhand der Zielsequenzen Hidden Markov Models in Queryproteinen suchen **DONE** 
* Beste Outputs ermitteln (Domänen) **Alle outputs in Heatmap**
* Koennt Ihr auf Basis des gegebenen phylogenetischen Baumes Hypothesen 
zu Gewinn oder Verlust bestimmter Domains aufstellen? **Heatmap**
 
#### Report schreiben 
* Methods durchgehen und abgleichen
    * Skripte in Attachments? Querformat?
* references
    * MBD2 sources **DONE**
* Results und Discussion/ Outlook
    * PFAM results
    * Heatmap
    * Hmmer
    * Trees, einen in Results, Rest in Attachment
    * Alignments (Robert)
* Korrekturlesen
*