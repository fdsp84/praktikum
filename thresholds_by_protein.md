## Thresholds for Coverage and E-Value by Protein

| Protein | Id | E-Value | Coverage  | 
|-----------------|:-------------|:---------------:|:---------------:|
| **MDB2** | AAC68871.1 | 6e-20 | 27%   |
| **MDB3** | AAH09372.1 | 5e-17 | 50% |
| **MTA1** | sp_Q13330.2_MTA1_HUMAN | 2e-32| 61% |
| **MTA2** | AAH53650.1 | 1e-30 | 45% |
| **MTA3** | sp_Q9BTC8.2_MTA3_HUMAN | 4e-23 | 69% |
| **RBBP4** | NP_005601.1 | 1e-21 | 29% |
| **RBBP7** | NP_001185648.1 | 1e-20 | 30% |