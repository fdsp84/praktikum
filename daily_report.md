### 22.06.2020

* [RBBP4/7](Results/Protein_Informations/RBBP4-7): Information about both proteins and correspondent FAA files
* [MBD2/3](Results/Protein_Informations/MBD2-3): Information about both proteins
* [MTA1/2/3](Results/Protein_Informations/MTA_1_2_3)
* [Script](src/proteome_download.py) for downloading proteomes automatically into [Proteome](Proteome) directory
* You do **not** need to run the script. We did it already on our praktikum server. All downloaded proteomes can be 
fetched from the praktikum server via **scp**. The password we got per email:

```
scp praktikum07@k44.bioinf.uni-leipzig.de:/scr/k44/praktikum07/praktikum/Proteome/all_proteome.tar.gz .
```
* [Here](Results/List_of_proteome_availability.txt) you find a list of available proteomes per species


Diese Fragen wurden noch hinzugefügt: (an die Betreuer: wir haben das nicht mitbekommen)

A) Welche Rolle spielen die ihnen zugewiesenen Proteine in der epigenetischen Regulation?

Welchen Komplexen gehoeren Sie an? Welche funktion haben Sie?

B) Kennzeichnen Sie im Speziesbaum MetazoaOri_topo die folgenden Taxa:
Bilateria, Choanoflagellata, Cnidaria,  Corallochytrea, Filasterea, Fungi, Ichthyosporea, Placozoa, Plants, Porifera, Teretrosporea

C) Welche Auswirkung koennten die Unterschiede der Datenquellen auf das Ergebnis haben?

D) Welches der ihnen zugewiesenen Proteine hat die meisten, unterschiedlichen Domänen?

Zu welchen davon gibt es PFAM modelle?


22.06.2020 Daily Report Sabine Vater
proteins to analyze: MTA1, 2 and 3
information and FASTA sequences found so far are in the corresponding file in Results
encountered problems with the technical aspects
 ToDo for the 23.06.2020: reading papers on MTAs more thoroughly, analyzing the FASTAs in the databases and answering the questions in the daily report (see above), discuss relationship between the three protein families for our group?, analyzing conserved domains of MTAs?

### 23.06.2020

#### Robert
* AA Sequencen von human MBD2/3 heruntergeladen (Humansequenz gut erforscht, gute Sequenzierungsqualität)
* SSH für Gitlab eingerichtet
* Einarbeitung in blastall
* [Diagramm](Results/diagrams) zur Visualisierung der Funktionen und Beziehungen der Proteine

#### Sabine
* vorläufiger BLAST run online gegen BLASTP, um kurz zu schauen, viele gute Treffer in Metazoen (siehe Results/MTA_1_2_3)
*Taxa in phylogenetischem Baum annotiert mit FigTree und NCBI Taxonomy (siehe MetazoaOri_topotree_annotated_23062020)
*versucht, SSH Verbindung zu Server herzustellen

#### Felipe

* Add protein isoforms in [RBBP4/7](Results/Protein_Informations/RBBP4-7)

##### [Script](src/proteome_database_creation.py) for creation of databases 
Right now, it processes compressed files, determining correct 
acronyms from proteomes, creating **faa** files with acronyms and **later** creating databases automatically.

* After downloading all proteomes:
```
scp praktikum07@k44.bioinf.uni-leipzig.de:/scr/k44/praktikum07/praktikum/Proteome/all_proteome.tar.gz .
```
* and extracting it `tar xfvz all_proteome.tar.gz` into directory **Proteome**
* go to `src` and run `python database_creation.py`
* right now you should get 47 files `.faa` as output
* for creating databases you should do it right now manually
* I'm having trouble runnning **formatdb** in python, see [Script](src/proteome_database_creation.py)

### 24.06.2020

## Felipe
* Fixed Bug in script for creation of database
* Created section [Database Creation](proteome_database_creation.md) with instructions on how to create databases
out of proteome compressed files
* Created [RBBP Proteins](Results/Protein_Informations/RBBP4-7/RBBP4-7-informations.md) information file mentioning chosen isoforms
* Updated [blastall queries](src/run_blast_commands.sh) for all proteins
* Generated blast results for all proteins, see `*.out` files in [MTA_1_2_3](Results/Protein_Informations/MTA_1_2_3),
[RBBP4-7](Results/Protein_Informations/RBBP4-7) and [MBD2-3](Results/Protein_Informations/MBD2-3)
* Big Blue button is down, right? I cannot enter the rooms

## Sabine
* downloaded proteomes, tried to get familiar with Linux commands
* researched e-values for BLAST runs, found paper with 1e-50 and in accordance with our results this will be the threshold
* tried to get familiar with ClustalW
* tonight looking through BLAST-Results for MTA1,2 and 3 created by Felipe 

## Robert
* resetup of project on linux laptop (instead of windows) because many reasons
* study blast results (-format)
* read up on Splitstree and think about next steps
* added [Blastall output format info](Results/Blastall output format info), tabellarisch wahrscheinlich sinnvoll für Weiterverarbeitung

### 25.06.2020

## Sabine
* took first protein for aligning BLAST-hits and constructing first phylogenetic tree
* got familiar with Splitstree
* trying to get familiar with HMMER (briefly)
* group meeting with Prof. Prohaska, Dr. Bernhardt, Matilda and Christoph; see MeetingNotes
* writing MeetingNotes

## Robert
* diagramm erweitert
* TODO: Domänen hinzufügen
* programme installiert

## Felipe
* Updated [blastall queries](src/run_blast_commands.sh) for all proteins with option 9
* Executed [blastall queries](src/run_blast_commands.sh) and saved results [here](Results/Blast_Outputs)
* Created [script](src/blast_result_database_creation.py) for creating postgres database table containing blast results
**without** the subject sequence
    * We still need a script for getting the sequences from the proteomes automatically
    * There is already a column for that in table `blast_results`
* Created [instructions](filling_up_database_table.md) for starting database running [script](src/blast_result_database_creation.py) 

### 26.06.2020

## Sabine
* looked through tasks for the first week and collected open tasks in Trello-Board
* filtered BLAST outputs regarding E-values and mismatch - alignment length ratio, but will be redone tomorrow using coverage calculation given by Dr. Weinberg
* briefly continued getting familiar with HMMER
* ToDo for the weekend: refiltering, HMMER, Splitstree, starting final report draft 

## Robert
* kein Erfolg beim Erstellen der Datenbank (Docker, postgres)
* Skrip umgeschrieben um mit pandas anstatt SQL zu arbeiten (wip)
* Besprechung mit Zasha über Coverage-Berechnung und Blastall Formate

## Felipe
* Created folder [Trees](Results/Trees) for organizing the root directory in our repository
* Moved information about which proteins we should handle into [Results](Results)
* Updated [blast_result_database_creation](src/blast_result_database_creation.py) by setting the fasta sequence of the subject protein
into the table `blast_results`
![DB Query Result With Fasta Sequence](database_query_result_with_sequence.png "DB Entries Example ")
* Removed **Postgres** and **Docker**. Now we use **SQLite** since it is very simple and only generates a file
    * The database is saved as a file `sqlite_database_file.db` in folder **src**
    when you run [blast_result_database_creation](src/blast_result_database_creation.py)
* **@Robert**: But you do not need to run [blast_result_database_creation](src/blast_result_database_creation.py) 
on your local machine if you want. I have already executed the script on our server **ssh praktikum07@k44.bioinf.uni-leipzig.de** 
in `/homes/praktikumserver3/praktikum07/praktikum/src`. The database file is already generated there.
    * If you want to run it locally, then you can access the file database with a SQL Client such as **Datagrip** 
* There is a **new script** for querying the database [blast_result_query](src/database_clustalw_pipeline.py).
Please use that to filter sequences for a query protein. As output all sequences with a smaller value than an **e-value**
are retrieved in **fasta** format (**sequences.out**). You have to add query protein by hand for the multple alignment.  
    * `python blast_result_query.py --protein-query-id 'sp|Q13330.2|MTA1_HUMAN' --e-value '7e-10'`
    * `python blast_result_query.py --p 'sp|Q13330.2|MTA1_HUMAN' --e '7e-10'` will also work
    * Try that on the server, folder `src`
* TODOS for the weekend:
    * implement coverage calculation 
    * multiple alignments either automatically with clustalo or by hand
    * splitstree
    * save all results in gitlab
    
## Robert
* Skript [get_protein_id_length](src/get_protein_id_length) (Ausführen mit "python get_protein_id_length.py)
* Coverage berechnen

### 29.06.2020
## Sabine/Felipe
* Created [TODOs](TODOs.md)
* Fixed coverage calculation
* Added coverage into main script, inserting it to the DB table
* Complement [database clustalw pipeline](src/database_clustalw_pipeline.py) by coverage
    * `python database_clustalw_pipeline.py --protein-query-id 'sp|Q13330.2|MTA1_HUMAN' --e-value '7e-10' --coverage 0.6`
    * `python database_clustalw_pipeline.py --p 'sp|Q13330.2|MTA1_HUMAN' --e '7e-10' --c 0.6` will also work
* Updated proteins dict with subject sequence and corrected query sequence lengths
* Added query sequence into sequences.out
* Run `clustalw -infile=sequences.out -align -type=PROTEIN -output=NEXUS`
* Checked Splitstree result
* Integrated **Clustalw** in the pipeline [blast_result_query](src/database_clustalw_pipeline.py)
* Now, running `python blast_result_query.py --protein-query-id 'sp|Q13330.2|MTA1_HUMAN' --e-value '7e-10' --coverage 0.6`
we get the filtered entries from the database and a **Clustalw** output: `sequences.nxs`

### 30.06.2020
## Sabine/Felipe
* [Table](thresholds_by_protein.md) with thresholds for each protein
* Determined duplicated or isoform [Sequences](Results/sequences_tobethrownout_30062020.txt) to be erased
* Changed [database creation script](src/blast_result_database_creation.py) for removing those sequences from the database
* Added [script](src/database_query.py) for querying database for later filtering manually
* Created [Multiple Alignments](Results/Candidate_Proteins) for all protein with Clustalwb
* Labelled phylogenetic trees, but Splitstree threw an error for RBBP4 and RBBP7
    RBBP4 shows one domain with WD40-like proteins and one with mostly ORFs, furthermore MTA1 and MTA3 as well as MBD2 
    and MBD3 show "interchangeable" sequences; definitely interesting - have to maybe look at iTOL

### 01.07.2020
## Sabine/Felipe
* [Table Species Script](src/acronym_species_name_table_creation.py) for later labeling of protein
* Remove nexus parameter from Clustawl in pipeline: we should generate `.aln` files, not `.nxs`
* Replace subject id with species name in [pipeline](src/database_clustalw_pipeline.py)
* [Running pipeline Script](src/run_pipeline_for_proteins.sh) for all proteins
* Add [Script](Results/Candidate_Proteins/changing_file_names.sh) for changing file names due to Windows probles
* Restructure folders in project and archive old files
* We get an error with Splitstree 5 ![Fehler Splitstree](fehler_splitstree_5.png "Error ")
* We get an error with Splitstree 4 ![Fehler Splitstree](fehler_splitstree_4.png "Error ")
* Add [Alignment files](Results/Alignments)
* Add [Nexus](Results) folder for generating trees with Splistree due to error by `.aln` files
* Add [Splistree Images](Results/Splitstree_Trees) of trees
* Began with final report in .doc format, raw outlinje with Methods section draft

### 02.07.2020
## Felipe
* Created [blast command script](src/run_blast_commands.sh) for running all Blast commands 
* Created [hmmsearch script](src/run_hmmsearch_for_proteins.sh) for running all Hmmer search combinations (domain x protein) 
    * Created [Hmmer search outputs](Results/HMM/Outputs)
* Created [heat map script](src/heat_map_creation.py) for generating domain heat maps for all proteins
* Generated [heat map images](Results/Heat_Maps_Domains) for all proteins

## Sabine
* pushed [ Final Report_newdraft_01072020.docx ] today due to problems with Git
* looked at all alignments in ClustalX2 and made notes as well as Screenshots
* started analysing phylogenetic networks and making screenshots (see Results/Splitstree_trees) as well as notes **continue here tomorrow**

## Robert
* Wrote introduction (incl diagram)
* Edit final report
* Aufarbeitung der letzten Tage (was wurde gemacht)

### 03.07.2020
## Felipe
* Created [README](README.md) as technical document
* Created [Heat Maps](Results/Heat_Maps_Domains) with biologically ordered species 

## Robert
* Diagram update
* work on Final Report

## Sabine
* made formatted phylogenetic networks and their pictures for the report (see Splitstree Notizen and "species not in metazoatree") 
* downloaded PFAM search results based on which the HMM profiles were downloaded for the report

## 04./05.07.
## Robert
* Final report: bearbeiten der heatmap, results
## 06.07. 
## Robert
 * Finished report
