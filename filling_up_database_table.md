##### Filling up database table

Now in order to fill in data into the table **blast_results** and **species**:
* Run `pip install -r requirements.txt`
* Run in **src** directory `python blast_result_database_creation.py`
    * We decided to remove [duplicated or isoform protein_subjects](Results/sequences_tobethrownout_30062020.txt) in 
    this [blast_result_database_creation.py](src/blast_result_database_creation.py)
* The file **sqlite_database_file.db** is created containing the database
    * Use a SQL client in order to check the table `blast_results`
* Run in **src** directory `python acronym_species_name_table_creation.py`
    * if you have already run [blast_result_database_creation.py](src/blast_result_database_creation.py),
    please comment line: `create_tables(database_cursor)` in [acronym_species_name_table_creation.py](src/acronym_species_name_table_creation.py)