##### [Script](src/proteome_database_creation.py) for creation of databases 
Right now, it processes compressed files, determining correct 
acronyms from proteomes, creating **faa** files with acronyms and creating databases automatically.

* After downloading all proteomes:
```
scp praktikum07@k44.bioinf.uni-leipzig.de:/scr/k44/praktikum07/praktikum/Proteome/all_proteome.tar.gz .
```
* and extracting it `tar xfvz all_proteome.tar.gz` into directory **Proteome**
* go to `src`
* open [Database creation script](src/proteome_database_creation.py)
* at line 100, replace `{path-to-your-formatdb}` with **your path** to the command `formatdb` 
* `python proteome_database_creation.py`
* right now you should get 47 files `.faa` as output
* and the databases: `phr`, `pin` and `psq` files

