import argparse
import subprocess
from database_configuration import get_database_connection
from blast_result_repository import select_sequences
from get_protein_id_length import get_query_fasta_sequence

OUT_FORMAT = '.out'


def _get_argument_parser():
    parser = argparse.ArgumentParser(description='Select sequence hits for protein query')
    parser.add_argument('--protein-query-id', type=str, required=True,
                        help='protein query id such as sp|Q13330.2|MTA1_HUMAN, take a look at proteins fasta file')
    parser.add_argument('--e-value', type=float, required=True,
                        help='e value in float or scientific notation if you want: 3e-04')
    parser.add_argument('--coverage', type=float, required=True,
                        help='coverage = /query_end - query_start)/query_length')
    return parser


def _get_sequence_with_species_name_as_id_and_species_name(sequence_tuple):
    fasta_sequence = sequence_tuple[0]
    fasta_sequence_id = fasta_sequence[:fasta_sequence.index(' ')].replace('>', '')
    fasta_sequence_with_species_name_as_id = '>' + sequence_tuple[2] + ' ' + fasta_sequence_id + ' ' +  fasta_sequence[fasta_sequence.index(' '):]
    return fasta_sequence_with_species_name_as_id, sequence_tuple[2]


def _write_unique_query_and_subject_sequences_with_species_as_id(output_file_name, query_sequence, sequence_tuples):
    with open(output_file_name, 'w') as de_output_file:
        species_set = set()
        de_output_file.write(query_sequence + '\n')
        for sequence_tuple in sequence_tuples:
            sequence, species_name = _get_sequence_with_species_name_as_id_and_species_name(sequence_tuple)
            if species_name not in species_set:
                species_set.add(species_name)
                de_output_file.write(sequence)


def _execute_clustalw(output_file_name):
    subprocess.call(['clustalw', '-infile=' + output_file_name, '-align', '-type=PROTEIN'])


def get_fasta_sequence_output_file_name(arguments):
    return arguments.protein_query_id + OUT_FORMAT


if __name__ == "__main__":
    parser = _get_argument_parser()
    arguments = parser.parse_args()

    try:
        database_connection = get_database_connection()
        database_cursor = database_connection.cursor()

        sequence_tuples = select_sequences(arguments.protein_query_id, arguments.e_value, arguments.coverage, database_cursor)

        query_id = sequence_tuples[0][1]

        query_sequence = get_query_fasta_sequence(query_id)

        output_file_name = get_fasta_sequence_output_file_name(arguments)

        _write_unique_query_and_subject_sequences_with_species_as_id(output_file_name, query_sequence, sequence_tuples)

        _execute_clustalw(output_file_name)
    finally:
        if database_connection is not None:
            database_connection.close()
