#!/bin/bash

python database_clustalw_pipeline.py  --p 'AAC68871.1' --e '6e-20' --c 0.27
python database_clustalw_pipeline.py  --p 'AAH09372.1' --e '5e-17' --c 0.5
python database_clustalw_pipeline.py  --p 'AAH53650.1' --e '1e-30' --c 0.45
python database_clustalw_pipeline.py  --p 'NP_001185648.1' --e '1e-20' --c 0.3
python database_clustalw_pipeline.py  --p 'NP_005601.1' --e '1e-21' --c 0.29
python database_clustalw_pipeline.py  --p 'sp|Q13330.2|MTA1_HUMAN' --e '2e-32' --c 0.61
python database_clustalw_pipeline.py  --p 'sp|Q9BTC8.2|MTA3_HUMAN' --e '4e-23' --c 0.69
