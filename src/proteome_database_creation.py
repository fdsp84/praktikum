from os import listdir
from os.path import isfile, join, splitext
import subprocess

CHOANOFLAGELLATA_GENERAL_FILE_URL = '2_choanoflagellate_transcriptomes.proteins.tgz'

CHOANOFLAGELLATA_ACRONYM = 'Choanoflagellata_General'

INPUT_PROTEOME_TABLE_FILE = '../Proteome/proteomTable.txt'
PROTEOME_DIRECTORY = '../Proteome/'

PROTEOM_COLUMN_NAME = 'Proteom'
PROTEOM_NOT_AVAILABLE_MESSAGE = 'Proteome not available for species:'
PROTEOM_NOT_AVAILABLE = '---'
COLUMN_SEPARATOR = '\t'
ZIP_EXTENSION = '.zip'
GZIP_EXTENSION = '.gz'
FASTA_EXTENSION = '.faa'
FASTA_WRITTEN_OUT_EXTENSION = '.fasta'


def _get_proteome_url_acronym_map():
    with open(INPUT_PROTEOME_TABLE_FILE, 'r') as proteome_table_file:
        proteome_url_acronym_map = {}
        file_lines = proteome_table_file.readlines()
        for file_line in file_lines:
            if COLUMN_SEPARATOR in file_line:
                line_columns = file_line.split(COLUMN_SEPARATOR)
                proteome_column = line_columns[3]
                if proteome_column == PROTEOM_NOT_AVAILABLE:
                    continue
                elif proteome_column != PROTEOM_COLUMN_NAME:
                    proteome_url_acronym_map[line_columns[3]] = line_columns[0]
        return proteome_url_acronym_map


def _get_species_acronym_map():
    with open(INPUT_PROTEOME_TABLE_FILE, 'r') as proteome_table_file:
        species_acronym_map = {}
        file_lines = proteome_table_file.readlines()
        for file_line in file_lines:
            if COLUMN_SEPARATOR in file_line:
                line_columns = file_line.split(COLUMN_SEPARATOR)
                proteome_column = line_columns[3]
                if proteome_column == PROTEOM_NOT_AVAILABLE or proteome_column != PROTEOM_COLUMN_NAME:
                    species_acronym_map[line_columns[1]] = line_columns[0]
        return species_acronym_map


def _get_input_file_names():
    return [f for f in listdir(PROTEOME_DIRECTORY) if isfile(join(PROTEOME_DIRECTORY, f))]


def extract_compressed_files(input_file_names, proteome_url_acronym_map):
    for input_file_name in input_file_names:
        for proteome_url in proteome_url_acronym_map.keys():
            if input_file_name in proteome_url:
                file_name, file_extension = splitext(input_file_name)
                output_fasta_file_name = proteome_url_acronym_map[proteome_url] + FASTA_EXTENSION

                with open(PROTEOME_DIRECTORY + output_fasta_file_name, 'w') as output_fasta_file:
                    if file_extension == ZIP_EXTENSION:
                        subprocess.call(['unzip', '-p', PROTEOME_DIRECTORY + input_file_name], stdout=output_fasta_file)
                    elif file_extension == GZIP_EXTENSION:
                        subprocess.call(['gunzip', '-c', PROTEOME_DIRECTORY + input_file_name], stdout=output_fasta_file)


def extract_choanoflagellata_bundle_compressed_file():
    subprocess.call(['tar', 'xvfz', PROTEOME_DIRECTORY + CHOANOFLAGELLATA_GENERAL_FILE_URL, '-C', PROTEOME_DIRECTORY])


def get_new_extracted_fasta_file_names():
    return [f for f in listdir(PROTEOME_DIRECTORY) if isfile(join(PROTEOME_DIRECTORY, f)) and splitext(f)[1] == FASTA_WRITTEN_OUT_EXTENSION]


def rename_new_extracted_fasta_files(new_extracted_fasta_file_names):
    for input_file_name in new_extracted_fasta_file_names:
        file_parts = input_file_name.split('_')
        acronym = (file_parts[1][0] + file_parts[2][0:3]).upper()
        subprocess.call(['mv', PROTEOME_DIRECTORY + input_file_name, PROTEOME_DIRECTORY + acronym + FASTA_EXTENSION])


if __name__ == "__main__":
    input_file_names = _get_input_file_names()

    proteome_url_acronym_map = _get_proteome_url_acronym_map()

    extract_compressed_files(input_file_names, proteome_url_acronym_map)

    extract_choanoflagellata_bundle_compressed_file()

    new_extracted_fasta_file_names = get_new_extracted_fasta_file_names()

    rename_new_extracted_fasta_files(new_extracted_fasta_file_names)

    faa_files = [f for f in listdir(PROTEOME_DIRECTORY) if isfile(join(PROTEOME_DIRECTORY, f)) and splitext(f)[1] == FASTA_EXTENSION]
    for faa_file in faa_files:
        print(PROTEOME_DIRECTORY + faa_file)
        # please change the path to your formatdb
        subprocess.call(['{path-to-your-formatdb}/formatdb', '-i', PROTEOME_DIRECTORY + faa_file])








