import argparse
from database_configuration import get_database_connection
from blast_result_repository import select_entries

OUT_FORMAT = '.out'


def _get_argument_parser():
    parser = argparse.ArgumentParser(description='Select sequence hits for protein query')
    parser.add_argument('--protein-query-id', type=str, required=True,
                        help='protein query id such as sp|Q13330.2|MTA1_HUMAN, take a look at proteins fasta file')
    parser.add_argument('--e-value', type=float, required=True,
                        help='e value in float or scientific notation if you want: 3e-04')
    return parser


def _write_out_first_sequence_line_and_parameters(output_file_name, result_tuples):
    with open(output_file_name, 'w') as query_output_file:
        for result_tuple in result_tuples:
            result_list = list(result_tuple)
            result_list[0] = get_first_line_of_fasta_sequence(result_tuple)
            query_output_file.write(str(result_list) + '\n')


def get_first_line_of_fasta_sequence(result_tuple):
    return result_tuple[0].split('\n')[0]


if __name__ == "__main__":
    parser = _get_argument_parser()
    arguments = parser.parse_args()

    try:
        database_connection = get_database_connection()
        database_cursor = database_connection.cursor()

        sequence_tuples = select_entries(arguments.protein_query_id, arguments.e_value, database_cursor)

        query_id = sequence_tuples[0][1]

        _write_out_first_sequence_line_and_parameters(arguments.protein_query_id + OUT_FORMAT, sequence_tuples)
    finally:
        if database_connection is not None:
            database_connection.close()
