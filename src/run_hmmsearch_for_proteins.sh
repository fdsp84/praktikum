#!/bin/bash

# RBBP4/7
hmmsearch --tblout ../Results/HMM/Outputs/WD40-RBBP4.hmmout ../Results/HMM/Profiles/WD40.hmm ../Results/Candidate_Proteins/NP_005601_1.out # RBBP4
hmmsearch --tblout ../Results/HMM/Outputs/WD40-RBBP7.hmmout ../Results/HMM/Profiles/WD40.hmm ../Results/Candidate_Proteins/NP_001185648_1.out # RBBP7

hmmsearch --tblout ../Results/HMM/Outputs/CAF1C_H4_bd-RBBP4.hmmout ../Results/HMM/Profiles/CAF1C_H4-bd.hmm ../Results/Candidate_Proteins/NP_005601_1.out # RBBP4
hmmsearch --tblout ../Results/HMM/Outputs/CAF1C_H4_bd-RBBP7.hmmout ../Results/HMM/Profiles/CAF1C_H4-bd.hmm ../Results/Candidate_Proteins/NP_001185648_1.out # RBBP7

# MTA1/2/3
hmmsearch --tblout ../Results/HMM/Outputs/MTA_R1-MTA1.hmmout ../Results/HMM/Profiles/MTA_R1.hmm ../Results/Candidate_Proteins/sp_Q13330_2_MTA1_HUMAN.out # MTA1
hmmsearch --tblout ../Results/HMM/Outputs/MTA_R1-MTA2.hmmout ../Results/HMM/Profiles/MTA_R1.hmm ../Results/Candidate_Proteins/AAH53650_1.out # MTA2
hmmsearch --tblout ../Results/HMM/Outputs/MTA_R1-MTA3.hmmout ../Results/HMM/Profiles/MTA_R1.hmm ../Results/Candidate_Proteins/sp_Q9BTC8_2_MTA3_HUMAN.out # MTA3

hmmsearch --tblout ../Results/HMM/Outputs/BAH-MTA1.hmmout ../Results/HMM/Profiles/BAH.hmm ../Results/Candidate_Proteins/sp_Q13330_2_MTA1_HUMAN.out # MTA1
hmmsearch --tblout ../Results/HMM/Outputs/BAH-MTA2.hmmout ../Results/HMM/Profiles/BAH.hmm ../Results/Candidate_Proteins/AAH53650_1.out # MTA2
hmmsearch --tblout ../Results/HMM/Outputs/BAH-MTA3.hmmout ../Results/HMM/Profiles/BAH.hmm ../Results/Candidate_Proteins/sp_Q9BTC8_2_MTA3_HUMAN.out # MTA3

hmmsearch --tblout ../Results/HMM/Outputs/ELM2-MTA1.hmmout ../Results/HMM/Profiles/ELM2.hmm ../Results/Candidate_Proteins/sp_Q13330_2_MTA1_HUMAN.out # MTA1
hmmsearch --tblout ../Results/HMM/Outputs/ELM2-MTA2.hmmout ../Results/HMM/Profiles/ELM2.hmm ../Results/Candidate_Proteins/AAH53650_1.out # MTA2
hmmsearch --tblout ../Results/HMM/Outputs/ELM2-MTA3.hmmout ../Results/HMM/Profiles/ELM2.hmm ../Results/Candidate_Proteins/sp_Q9BTC8_2_MTA3_HUMAN.out # MTA3

hmmsearch --tblout ../Results/HMM/Outputs/GATA-MTA1.hmmout ../Results/HMM/Profiles/GATA.hmm ../Results/Candidate_Proteins/sp_Q13330_2_MTA1_HUMAN.out # MTA1
hmmsearch --tblout ../Results/HMM/Outputs/GATA-MTA2.hmmout ../Results/HMM/Profiles/GATA.hmm ../Results/Candidate_Proteins/AAH53650_1.out # MTA2
hmmsearch --tblout ../Results/HMM/Outputs/GATA-MTA3.hmmout ../Results/HMM/Profiles/GATA.hmm ../Results/Candidate_Proteins/sp_Q9BTC8_2_MTA3_HUMAN.out # MTA3

hmmsearch --tblout ../Results/HMM/Outputs/Myb_DNA_binding-MTA1.hmmout ../Results/HMM/Profiles/Myb_DNA_binding.hmm ../Results/Candidate_Proteins/sp_Q13330_2_MTA1_HUMAN.out # MTA1

# MBD2/3
hmmsearch --tblout ../Results/HMM/Outputs/MBD_C-MBD2.hmmout ../Results/HMM/Profiles/MBD_C.hmm ../Results/Candidate_Proteins/AAC68871_1.out # MBD2
hmmsearch --tblout ../Results/HMM/Outputs/MBD_C-MBD3.hmmout ../Results/HMM/Profiles/MBD_C.hmm ../Results/Candidate_Proteins/AAH09372_1.out # MBD3

hmmsearch --tblout ../Results/HMM/Outputs/MBDa-MBD2.hmmout ../Results/HMM/Profiles/MBDa.hmm ../Results/Candidate_Proteins/AAC68871_1.out # MBD2
hmmsearch --tblout ../Results/HMM/Outputs/MBDa-MBD3.hmmout ../Results/HMM/Profiles/MBDa.hmm ../Results/Candidate_Proteins/AAH09372_1.out # MBD3

hmmsearch --tblout ../Results/HMM/Outputs/MBD-MBD2.hmmout ../Results/HMM/Profiles/MBD.hmm ../Results/Candidate_Proteins/AAC68871_1.out # MBD2