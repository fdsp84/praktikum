from os import listdir
from os.path import isfile, join, splitext
from database_configuration import get_database_connection, create_tables
from blast_result_repository import insert_blast_result, update_subject_fasta_sequence_for_subject_id, delete_sequences_by_query_id_and_subject_id
from Bio import SeqIO
from Bio.SeqIO.FastaIO import as_fasta
from get_protein_id_length import get_length

BLAST_OUTPUTS_INPUT_DIRECTORY = '../Results/Blast_Outputs/'
PROTEOME_INPUT_DIRECTORY = '../Proteome/'
TAB_CHARACTER = '\t'
FASTA_EXTENSION = '.faa'
FASTA_FORMAT = 'fasta'
GENERAL_DUPLICATED_OR_ISOFORM_PROTEIN_SUBJECTS_TO_BE_REMOVED_FILE = '../Results/sequences_tobethrownout_30062020.txt'


def read_subject_ids_to_be_deleted():
    with open(GENERAL_DUPLICATED_OR_ISOFORM_PROTEIN_SUBJECTS_TO_BE_REMOVED_FILE) as input_file:
        content = input_file.readlines()
    return [x.strip() for x in content]


def update_subject_sequences(subject_ids, database_cursor):
    subject_id_fasta_sequence_dict = {}
    subject_id_abbreviation_dict = {}
    proteome_file_names = [f for f in listdir(PROTEOME_INPUT_DIRECTORY) if isfile(join(PROTEOME_INPUT_DIRECTORY, f)) and splitext(f)[1] == FASTA_EXTENSION]
    for proteome_file_name in proteome_file_names:
        fasta_sequences = SeqIO.parse(open(PROTEOME_INPUT_DIRECTORY + proteome_file_name), FASTA_FORMAT)
        for fasta_sequence in fasta_sequences:
            for subject_id in subject_ids:
                if fasta_sequence.id == subject_id:
                    subject_id_fasta_sequence_dict[subject_id] = as_fasta(fasta_sequence)
                    subject_id_abbreviation_dict[subject_id] = splitext(proteome_file_name)[0]

    for subject_id in subject_id_fasta_sequence_dict.keys():
        update_subject_fasta_sequence_for_subject_id(subject_id, subject_id_fasta_sequence_dict[subject_id], subject_id_abbreviation_dict[subject_id], database_cursor)


def _get_coverage(query_id, query_start, query_end):
    query_sequence_length = get_length(query_id)
    return (query_end - query_start)/query_sequence_length


def process_blast_output_file_and_persist_entries(input_file_name):
    with open(BLAST_OUTPUTS_INPUT_DIRECTORY + input_file_name, 'r') as input_file:
        file_lines = input_file.readlines()
        for file_line in file_lines:
            if TAB_CHARACTER in file_line:
                file_line = file_line.replace('\n', '')
                line_columns = file_line.split(TAB_CHARACTER)

                print(line_columns)

                subject_ids.add(line_columns[1])
                coverage = _get_coverage(line_columns[0], int(line_columns[6]), int(line_columns[7]))

                insert_blast_result(line_columns[0],
                                    line_columns[1],
                                    float(line_columns[2]),
                                    int(line_columns[3]),
                                    int(line_columns[4]),
                                    int(line_columns[5]),
                                    int(line_columns[6]),
                                    int(line_columns[7]),
                                    int(line_columns[8]),
                                    int(line_columns[9]),
                                    float(line_columns[10]),
                                    float(line_columns[11]),
                                    coverage,
                                    database_cursor)


if __name__ == "__main__":
    try:
        database_connection = get_database_connection()
        database_cursor = database_connection.cursor()
        create_tables(database_cursor) # only execute it one time!

        input_file_names = [f for f in listdir(BLAST_OUTPUTS_INPUT_DIRECTORY) if isfile(join(BLAST_OUTPUTS_INPUT_DIRECTORY, f))]

        subject_ids = set()

        for input_file_name in input_file_names:
            process_blast_output_file_and_persist_entries(input_file_name)
            database_connection.commit()

        update_subject_sequences(subject_ids, database_cursor)
        database_connection.commit()

        print('reading subject ids to be excluded')
        subject_ids_to_be_removed = read_subject_ids_to_be_deleted()
        print('deleting subject ids')
        delete_sequences_by_query_id_and_subject_id(subject_ids_to_be_removed, database_cursor)

        database_connection.commit()
    finally:
        if database_connection is not None:
            database_connection.close()
