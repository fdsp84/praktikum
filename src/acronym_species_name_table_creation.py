from os import listdir
from os.path import isfile, join, splitext
from database_configuration import get_database_connection, create_tables
from blast_result_repository import insert_species

CHOANOFLAGELLATA_GENERAL_FILE_URL = '2_choanoflagellate_transcriptomes.proteins.tgz'

CHOANOFLAGELLATA_ACRONYM = 'Choanoflagellata_General'

INPUT_PROTEOME_TABLE_FILE = '../Proteome/proteomTable.txt'
NEW_PROTEOME_DIRECTORY = '../Proteome/New_Proteomes'

PROTEOM_COLUMN_NAME = 'Proteom'
PROTEOM_NOT_AVAILABLE_MESSAGE = 'Proteome not available for species:'
PROTEOM_NOT_AVAILABLE = '---'
COLUMN_SEPARATOR = '\t'
ZIP_EXTENSION = '.zip'
GZIP_EXTENSION = '.gz'
FASTA_EXTENSION = '.faa'
FASTA_WRITTEN_OUT_EXTENSION = '.fasta'



def _get_acronym_species_map():
    with open(INPUT_PROTEOME_TABLE_FILE, 'r') as proteome_table_file:
        acronym_species_map = {}
        file_lines = proteome_table_file.readlines()
        for file_line in file_lines:
            if COLUMN_SEPARATOR in file_line:
                line_columns = file_line.split(COLUMN_SEPARATOR)
                proteome_column = line_columns[3]
                if proteome_column == PROTEOM_NOT_AVAILABLE or proteome_column != PROTEOM_COLUMN_NAME:
                    acronym_species_map[line_columns[0]] = line_columns[1]
        return acronym_species_map


def get_new_extracted_fasta_file_names():
    return [f for f in listdir(NEW_PROTEOME_DIRECTORY) if isfile(join(NEW_PROTEOME_DIRECTORY, f)) and splitext(f)[1] == FASTA_WRITTEN_OUT_EXTENSION]


def get_acronym_species_map_from_new_extracted_proteomes(new_extracted_fasta_file_names):
    acronym_species_map = {}
    for input_file_name in new_extracted_fasta_file_names:
        file_parts = input_file_name.split('_')
        acronym = (file_parts[1][0] + file_parts[2][0:3]).upper()
        species = file_parts[1] + ' ' + file_parts[2].replace('.proteins.fasta', '')
        acronym_species_map[acronym] = species
    return acronym_species_map


if __name__ == "__main__":
    try:
        database_connection = get_database_connection()
        database_cursor = database_connection.cursor()
        create_tables(database_cursor)  # only execute it one time!

        acronym_map = _get_acronym_species_map()

        new_extracted_fasta_file_names = get_new_extracted_fasta_file_names()

        acronym_map_from_new_files = get_acronym_species_map_from_new_extracted_proteomes(new_extracted_fasta_file_names)

        acronym_map.update(acronym_map_from_new_files)

        for acronym in acronym_map.keys():
            species_name_with_underline = acronym_map[acronym].replace(' ', '_')
            insert_species(acronym, species_name_with_underline, database_cursor)
            print(acronym, species_name_with_underline)

        database_connection.commit()

    finally:
        if database_connection is not None:
            database_connection.close()










