from os import listdir
from os.path import isfile, join, splitext
import numpy
import matplotlib.pyplot as plt
from heat_map import heat_map, annotate_heatmap
from heat_map import annotate_heatmap
import math

ALL_PROTEINS = 'All Proteins'

SPECIES_NOT_PRESENT = 0

SPECIES_VALUES_KEY = 'species_values'

DOMAIN_KEY = 'domain'

HMMER_PROTEIN_DOMAIN_SEPARATOR = '-'
HMMER_OUTPUTS_INPUT_DIRECTORY = '../Results/HMM/Outputs/'
COMMENT_CHARACTER = '#'
HOMO_SAPIENS_SPECIES = 'Homo sapiens'
HOMO_SAPIENS_PROTEINS = ['MBD2', 'MBD3', 'MTA2', 'NP_001185648.1', 'NP_005601.1', 'sp|Q13330.2|MTA1_HUMAN', 'sp|Q9BTC8.2|MTA3_HUMAN']

RBBP = ['RBBP4', 'RBBP7']
MBD = ['MB2', 'MBD3']
MTA = ['MTA1', 'MTA2', 'MTA3']

FIGURE_SIZE = {'MTA1': (12, 6), 'MTA2': (14, 6), 'MTA3': (14, 6), 'RBBP4': (30, 4), 'RBBP7': (25, 4),'MBD2': (15, 5), 'MBD3': (12, 5), ALL_PROTEINS: (30, 10)}

COLOR_MAPS = ['YlGn', 'Purples', 'Blues', 'Reds', 'PuBu', 'Oranges', 'Greens']


def get_species(species):
    if species in HOMO_SAPIENS_PROTEINS:
        return HOMO_SAPIENS_SPECIES
    else:
        return species.replace('_', ' ')


def get_proteins_with_domains_species_and_e_values(input_file_names):
    proteins = {}
    for input_file_name in input_file_names:
        with open(HMMER_OUTPUTS_INPUT_DIRECTORY + input_file_name, 'r') as input_file:
            file_lines = input_file.readlines()
            file_name_without_extension = splitext(input_file_name)[0]
            protein = file_name_without_extension.split(HMMER_PROTEIN_DOMAIN_SEPARATOR)[1]

            species_dict = {}

            for file_line in file_lines:
                if COMMENT_CHARACTER != file_line[0]:
                    columns = file_line.split()

                    domain = (columns[2])
                    raw_species = columns[0]
                    species = get_species(raw_species)

                    species_dict[species] = float(columns[4])

            if protein not in proteins:
                proteins[protein] = []
            proteins[protein].append({DOMAIN_KEY: domain, SPECIES_VALUES_KEY: species_dict})
    return proteins


def get_sorted_species_for_protein(protein, species_in_bio_order):
    species_for_protein = set()
    for domain_infos in protein:
        species_for_domain = set(domain_infos[SPECIES_VALUES_KEY].keys())
        species_for_protein.update(species_for_domain)

    sorted_species_for_protein = list()
    for species in species_in_bio_order:
        if species in species_for_protein:
            sorted_species_for_protein.append(species)

    return sorted_species_for_protein


def create_image_protein(protein_index, protein, numpy_protein_values, protein_domain_rows, sorted_species_for_protein):
    figure, axes = plt.subplots()
    figure.suptitle(protein)
    im, cbar = heat_map(numpy_protein_values, protein_domain_rows, sorted_species_for_protein, ax=axes,
                        cmap='Blues', cbarlabel="normalized log(expect_value)")
    #texts = annotate_heatmap(im, valfmt="{x:.1f}")

    figure.tight_layout()
    figure = plt.gcf()  # get current figure

    figure.set_size_inches(FIGURE_SIZE[protein])
    plt.savefig(protein, dpi=180)


def get_species_in_bio_order():
    return list(line.strip().replace('_', ' ') for line in open('../Results/Heat_Maps_Domains/Species_List_for_Heatmap.txt'))


if __name__ == '__main__':

    species_in_bio_order = get_species_in_bio_order()

    input_file_names = [f for f in listdir(HMMER_OUTPUTS_INPUT_DIRECTORY) if isfile(join(HMMER_OUTPUTS_INPUT_DIRECTORY, f))]

    proteins = get_proteins_with_domains_species_and_e_values(input_file_names)

    protein_domain_rows = []
    protein_values = []
    for protein_index, protein in enumerate(proteins, start=0):

        for domain_infos in proteins[protein]:
            protein_domain_rows.append(domain_infos[DOMAIN_KEY])
            protein_domain_values = []
            for species in species_in_bio_order:
                if species in domain_infos[SPECIES_VALUES_KEY]:
                    e_value = math.log10(domain_infos[SPECIES_VALUES_KEY][species])
                    protein_domain_values.append(e_value)
                else:
                    protein_domain_values.append(SPECIES_NOT_PRESENT)

            numpy_protein_domain_values = numpy.array(protein_domain_values)
            norm_protein_domain_values = numpy_protein_domain_values / numpy.linalg.norm(numpy_protein_domain_values)
            protein_values.append(list(norm_protein_domain_values))

    numpy_protein_values = numpy.array(protein_values)
    create_image_protein(protein_index, ALL_PROTEINS, numpy_protein_values, protein_domain_rows, species_in_bio_order)



