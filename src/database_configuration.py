import sqlite3

CREATE_TABLE_FILE = '../database/init.sql'


def get_database_connection():
    print("Successfully Connected to SQLite")
    return sqlite3.connect('sqlite_database_file.db')


def create_tables(database_cursor):
    with open(CREATE_TABLE_FILE, 'r') as create_tables_file:
        create_table_script = create_tables_file.read()
        database_cursor.executescript(create_table_script)
        print("SQLite create table script executed successfully")

