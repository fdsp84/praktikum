import subprocess

INPUT_PROTEOME_TABLE_FILE = '../Proteome/proteomTable.txt'
OUTPUT_DIRECTORY = '../Proteome/'
PROTEOM_COLUMN_NAME = 'Proteom'
PROTEOM_NOT_AVAILABLE_MESSAGE = 'Proteome not available for species:'
PROTEOM_NOT_AVAILABLE = '---'
COLUMN_SEPARATOR = '\t'


if __name__ == "__main__":
    with open(INPUT_PROTEOME_TABLE_FILE, 'r') as proteome_table_file:
        file_lines = proteome_table_file.readlines()
        for file_line in file_lines:
            if COLUMN_SEPARATOR in file_line:
                line_columns = file_line.split(COLUMN_SEPARATOR)
                proteome_column = line_columns[3]
                if proteome_column == PROTEOM_NOT_AVAILABLE:
                    print(PROTEOM_NOT_AVAILABLE_MESSAGE, line_columns[1])
                elif proteome_column != PROTEOM_COLUMN_NAME:
                    subprocess.call(['wget', '-P', OUTPUT_DIRECTORY, proteome_column])
