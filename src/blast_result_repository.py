from sqlite3 import Error

INSERT_BLAST_RESULT_SQL = """INSERT INTO blast_results( query_id,
                                                        subject_id,
                                                        identity,
                                                        alignment_length,
                                                        mismatches,
                                                        gap_openings,
                                                        query_start,
                                                        query_end,
                                                        subject_start,
                                                        subject_end,
                                                        expect_value,
                                                        bit_score,
                                                        coverage) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"""

INSERT_SPECIES_SQL = """INSERT INTO species( acronym, name ) VALUES(?, ?);"""

UPDATE_SUBJECT_SEQUENCE_SQL = """UPDATE blast_results SET subject_fasta_sequence = ?, abbreviation = ? WHERE subject_id = ?;"""

SELECT_SEQUENCES_BY_PROTEIN_QUERY_E_VALUE_AND_COVERAGE = """
SELECT subject_fasta_sequence, query_id, species.name
FROM blast_results
JOIN species ON blast_results.abbreviation = species.acronym 
WHERE query_id = ? 
AND expect_value <= ? 
AND coverage >= ?;
"""

DELETE_SEQUENCES_PROTEIN_SUBJECT = """
DELETE 
FROM blast_results 
WHERE subject_id IN {};
"""

SELECT_ENTRIES_BY_PROTEIN_QUERY_E_VALUE_AND_COVERAGE = """
SELECT subject_fasta_sequence, subject_id, query_id, coverage, expect_value
FROM blast_results 
WHERE query_id = ? 
AND expect_value <= ? 
ORDER BY expect_value ASC;
"""


def insert_blast_result(query_id,
                        subject_id,
                        identity,
                        alignment_length,
                        mismatches,
                        gap_openings,
                        query_start,
                        query_end,
                        subject_start,
                        subject_end,
                        expect_value,
                        bit_score,
                        coverage,
                        database_cursor):
    try:
        database_cursor.execute(INSERT_BLAST_RESULT_SQL, (query_id,
                                                          subject_id,
                                                          identity,
                                                          alignment_length,
                                                          mismatches,
                                                          gap_openings,
                                                          query_start,
                                                          query_end,
                                                          subject_start,
                                                          subject_end,
                                                          expect_value,
                                                          bit_score,
                                                          coverage))
    except Error as error:
        print(error)


def insert_species(acronym, species_name, database_cursor):
    try:
        database_cursor.execute(INSERT_SPECIES_SQL, (acronym, species_name))
    except Error as error:
        print(error)


def update_subject_fasta_sequence_for_subject_id(subject_id, subject_fasta_sequence, abbreviation, database_cursor):
    try:
        database_cursor.execute(UPDATE_SUBJECT_SEQUENCE_SQL, (subject_fasta_sequence, abbreviation, subject_id))
    except Error as error:
        print(error)


def select_sequences(protein_query_id, e_value, coverage, database_cursor):
    try:
        database_cursor.execute(SELECT_SEQUENCES_BY_PROTEIN_QUERY_E_VALUE_AND_COVERAGE, (protein_query_id, e_value, coverage))
        return database_cursor.fetchall()
    except Error as error:
        print(error)


def delete_sequences_by_query_id_and_subject_id(subject_ids, database_cursor):
    try:
        delete_subject_ids_statement = DELETE_SEQUENCES_PROTEIN_SUBJECT.format(tuple(subject_ids))
        database_cursor.execute(delete_subject_ids_statement, ())
    except Error as error:
        print(error)


def select_entries(protein_query, e_value, database_cursor):
    try:
        database_cursor.execute(SELECT_ENTRIES_BY_PROTEIN_QUERY_E_VALUE_AND_COVERAGE, (protein_query, e_value))
        return database_cursor.fetchall()
    except Error as error:
        print(error)
