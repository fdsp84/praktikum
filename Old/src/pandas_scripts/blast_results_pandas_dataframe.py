import pandas as pd
import numpy as np
from os import listdir
from os.path import isfile, join
from get_protein_id_length import get_proteins, get_length
import sys

INPUT_DIRECTORY = '../../Results/Blast_Outputs/'
OUTPUT_DIRECTORY = '../../Results/Blastout_dataframes/'
TAB_CHARACTER = '\t'
PROTEINS = get_proteins()

def clean_field_names(list):
    list[0] = list[0].split(": ")[1]
    list[-1] = "bit score"
    return list

def insert_line(cell):
    pass

def float_parser(str):
    if "." in str:
        return [0,1]
    else:
        split = str.split("e")
        split[0] = int(split[0])
        split[1] = int(split[1])
        return split

if __name__ == "__main__":

    input_file_names = [f for f in listdir(INPUT_DIRECTORY) if isfile(join(INPUT_DIRECTORY, f))]

    for input_file_name in input_file_names:
        made_df = False
        with open(INPUT_DIRECTORY + input_file_name, 'r') as input_file:
            file_lines = input_file.readlines()
            first_run = True
            for file_line in file_lines:
                if "Fields" in file_line:
                    fields = clean_field_names(file_line.split(", "))
                    df = pd.DataFrame(columns=fields)
                    made_df = True
                if TAB_CHARACTER in file_line:
                    file_line = file_line.replace('\n', '')
                    line_columns = file_line.split(TAB_CHARACTER)
                    #print(line_columns)
                    # Columns: [Query id, Subject id, % identity, alignment length, mismatches, gap openings, q. start, q. end, s. start, s. end, e-value, bit score]
                    row = pd.Series([line_columns[0],
                                        line_columns[1],
                                        float(line_columns[2]),
                                        int(line_columns[3]),
                                        int(line_columns[4]),
                                        int(line_columns[5]),
                                        int(line_columns[6]),
                                        int(line_columns[7]),
                                        int(line_columns[8]),
                                        int(line_columns[9]),
                                        float_parser(line_columns[10]), # es soll [2,-60] stehen
                                        float(line_columns[11]),
                                        ], index=df.columns)
                    df = df.append(row, ignore_index=True)
        df["coverage"] = [df.iloc[i,3]/get_length(df.iloc[i,0]) for i in df.index] #alignment length/query length
        if first_run:
            df.to_csv(OUTPUT_DIRECTORY + "all_proteins" + ".csv", index=False)
            first_run = False
        else:
            df.to_csv(OUTPUT_DIRECTORY + "all_proteins" + ".csv", index=False, columns=False, mode='a')