import pandas as pd
from os import listdir
from os.path import isfile, join
import sys
from get_protein_id_length import get_proteins, get_length, get_identifier

params = sys.argv[1:]
if params == []:
    print("Usage: first specify protein name, then all the columns you want printed\n"
          "Example: python blast_result_query_pandas.py MBD2 coverage \"Subject id\"\n")

INPUT_DIRECTORY = '../../Results/Blastout_dataframes/'

input_file_names = [f for f in listdir(INPUT_DIRECTORY) if isfile(join(INPUT_DIRECTORY, f))]

def load_df():
    df = pd.read_csv(INPUT_DIRECTORY + "all_proteins.csv")
    return df

def filter(data, params):
    df = data
    name = params[0]
    print(name)
    cols = params[1:]
    print("filter")
    ident = get_identifier(name)
    print (ident, cols)
    # try:
    # return df[df["Query id]" == ident]][[f"{col}" for col in cols]]
    return df[df['Query id']==ident]
    # except:
    #     print("error")

if __name__ == "__main__":
    df = load_df()
    if len(params) <= 1:
        # print("\n")
        print(df.head(2))
    if len(params) == 1:
        print("specify colums")
    if len(params) >     1:
        df = filter(df, params)
        print(df)
