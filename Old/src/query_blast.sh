#!/usr/bin/bash
query_name=$1
evalue=$2
echo "query_protein: "$query_name | tee temp_query_blast
query_protein_id=$(python get_protein_id_length.py MBD2 identifier)
echo "protein identifier: "$query_protein_id | tee -a temp_query_blast
echo "e-vlaue threshold: "$evalue | tee -a temp_query_blast
python database_clustalw_pipeline.py --p $query_protein_id --e $evalue | tee -a temp_query_blast
echo "hits: "$(cat sequences.out | grep -c ">") | tee -a temp_query_blast
# write info to file:
cat temp_query_blast >../Results/Clustalo_Input/query_output_${query_name}_${query_protein_id}_${evalue}_meta.txt
# write query sequence to file: ...
# write sequence.out to file:
cat sequences.out >>../Results/Clustalo_Input/query_output_${query_name}_${query_protein_id}_${evalue}.fasta
